Modules extend pager functionality.

INSTALLATION INSTRUCTION
--------------------------------

For Node page with pager.
Modify metatags fields as below:
* title:	[current-page:title] [seo_pager:page-number]
* canonical_url:	[current-page:url:with-query]

For Taxonomy page with pager.
Modify metatags fields for taxonomy terms on /admin/config/search/metatag page as below:
* title:	[term:name] [seo_pager:page-number]
* description:	[term:description] [seo_pager:page-number]
* canonical_url:	[current-page:url:with-query]

For Views page with pager.
Modify metatags on views edit page (provided metatag_views module) as below:
* title:	[site:name] [seo_pager:page-number]
* description:	[site:name] [seo_pager:page-number]
* canonical_url:	[current-page:url:with-query]
