<?php

/**
 * @file
 * Token callbacks for the seo_pager module.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function seo_pager_token_info() {

  $info['tokens']['url']['with-query'] = [
    'name' => t('Absolute URL with query'),
    'description' => t('Absolute URL with query.'),
  ];

  return $info;
}

/**
 * Implements hook_tokens().
 */
function seo_pager_tokens($type,
                          array $tokens,
                          array $data,
                          array $options,
                          BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  // URL tokens.
  if ($type == 'url' && !empty($data['url'])) {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Add token [current-page:url:with-query]
        // which return full path with query.
        case 'with-query':
          $current_uri = \Drupal::request()->getUri();
          $bubbleable_metadata->addCacheableDependency($current_uri);
          $replacements[$original] = $current_uri;
          break;
      }
    }
  }

  if ($type == 'seo_pager') {
    $request = \Drupal::request();
    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Add [seo_pager:page-number] token.
        case 'page-number':
          $result = '';
          if ($page = $request->query->get('page')) {
            $page_number = (int) $page + 1;

            $current_uri = \Drupal::request()->getUri();
            $bubbleable_metadata->addCacheableDependency($current_uri);
            // Return text " - page <page_number>".
            $result = $page_number ? ' - ' . mb_strtolower(t('Page')) . ' ' . $page_number : '';
          }
          $replacements[$original] = $result;
          break;
      }
    }
  }

  return $replacements;
}
