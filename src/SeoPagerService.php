<?php

namespace Drupal\seo_pager;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\mysql\Driver\Database\mysql\Connection;
use Drupal\views\Views;

/**
 * Class SeoPagerService implementation.
 */
class SeoPagerService {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\mysql\Driver\Database\mysql\Connection definition.
   *
   * @var \Drupal\mysql\Driver\Database\mysql\Connection
   */
  protected $database;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new NicePagerService object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, Connection $database, MessengerInterface $messenger) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
    $this->messenger = $messenger;
  }

  /**
   * Check if current page is on front side page.
   */
  public function isFrontSide() {
    // Ignore admin paths.
    if (\Drupal::service('router.admin_context')->isAdminRoute()) {
      return FALSE;
    }

    // Skip everything if the site is in maintenance mode.
    $route_match = \Drupal::routeMatch();
    if (\Drupal::service('maintenance_mode')->applies($route_match)) {
      return FALSE;
    }

    $front_theme = \Drupal::config('system.theme');
    $active_theme = \Drupal::service('theme.manager')->getActiveTheme();
    // Check only front side views path.
    return ($front_theme->get('default') == $active_theme->getName());
  }

  /**
   * Return views list with full pager.
   */
  public function getViewsWithPager() {
    $result = [];
    foreach (Views::getEnabledViews() as $view_name => $view) {
      foreach ($view->get('display') as $view_display => $display) {
        $pager_type = $display["display_options"]["pager"]['type'] ?? '';
        if (in_array($pager_type, ['mini', 'full'])) {
          $result[] = $view_name . ($view_display == 'default' ? '' : '/' . $view_display);
        }
      }
    }
    return $result;
  }

  /**
   * Return url for pager button without page=0 variable.
   */
  public function getQueryWithoutPageZero(string $page_href) {
    $args = $this->getArgsWithoutPageZero($page_href);
    // There is no page=0 in query string.
    if ($args === FALSE) {
      return $page_href;
    }
    // If path was just ?page=0.
    if (empty($args)) {
      // Return aliased current path without query string.
      $current_path = \Drupal::service('path.current')->getPath();
      return \Drupal::service('path_alias.manager')->getAliasByPath($current_path);
    }
    // Returned array of args without page=0.
    $page_href = strpos($page_href, '?') !== FALSE ? '?' : '';
    $page_href .= http_build_query($args);
    return $page_href;
  }

  /**
   * Return query args without page=0 variable.
   */
  public function getArgsWithoutPageZero(string $page_href) {
    if ($page_href) {
      if (strpos($page_href, '?') === FALSE) {
        $page_href .= '?';
      }
      $url_info = parse_url($page_href);
      $args = [];
      parse_str($url_info['query'], $args);

      if (isset($args['page']) && $args['page'] == '0') {
        // Remove page=0 from query.
        unset($args['page']);
        // Return args array or current path.
        return $args;
      }
    }
    return FALSE;
  }

}
