<?php

namespace Drupal\seo_pager\EventSubscriber;

use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\seo_pager\SeoPagerService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Implements redirection from pages with page=0 in query.
 */
class TaxonomyPagerRedirectSubscriber implements EventSubscriberInterface {

  /**
   * The request stack.
   *
   * @var \Drupal\seo_pager\SeoPagerService
   */
  protected $seoPager;

  /**
   * Constructs a TaxonomyPagerRedirectSubscriber object.
   */
  public function __construct(SeoPagerService $seoPagerService) {
    $this->seoPager = $seoPagerService;
  }

  /**
   * Kernel request event handler.
   */
  public function onKernelRequest(RequestEvent $event): void {
    $request = $event->getRequest();
    // Get URL info and process it to be used for hash generation.
    $args = $request->query->all();
    if (empty($args) || !isset($args['page']) || $args['page'] !== '0') {
      return;
    }

    // Check only front side views.
    if (!$this->seoPager->isFrontSide()) {
      return;
    }

    // Remove pager=0 from query.
    unset($args['page']);

    $url = Url::createFromRequest($request);
    $url->setOption('query', (array) $url->getOption('query') + $args);

    $response = new TrustedRedirectResponse($url->setAbsolute()
      ->toString(), 301);
    $response->addCacheableDependency($url);
    $event->setResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::REQUEST => ['onKernelRequest'],
    ];
  }

}
